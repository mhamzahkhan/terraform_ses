resource "aws_ses_domain_identity" "this" {
  for_each = toset(var.domains)
  domain   = each.key
}

data "cloudflare_zone" "this" {
  for_each = toset(var.domains)
  name     = each.key
}

resource "cloudflare_record" "verification" {
  for_each = aws_ses_domain_identity.this

  zone_id = data.cloudflare_zone.this[each.key].id
  name    = "_amazonses"
  value   = each.value.verification_token
  type    = "TXT"
}

module "dkim" {
   source = "./modules/dkim"

   for_each = data.cloudflare_zone.this

   domain = each.value.name
   zone_id = each.value.zone_id
}
