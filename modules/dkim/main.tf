resource "aws_ses_domain_dkim" "this" {
  domain   = var.domain
}

resource "cloudflare_record" "dkim" {
  count   = 3
  zone_id = var.zone_id
  name    = "${aws_ses_domain_dkim.this.dkim_tokens[count.index]}._domainkey"
  value   = "${aws_ses_domain_dkim.this.dkim_tokens[count.index]}.dkim.amazonses.com"
  type    = "TXT"
}
