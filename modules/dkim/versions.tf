terraform {
  required_version = ">= 1.1.7"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.37.0"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.24.0"
    }
  }
}
