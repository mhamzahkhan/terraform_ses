variable "domain" {
  type        = string
  description = "Domain Name"
}

variable "zone_id" {
  type = string
}
